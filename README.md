
[![Netlify Status](https://api.netlify.com/api/v1/badges/88202a02-e6a0-428b-a596-ac89c549d7e3/deploy-status)](https://app.netlify.com/sites/wizardly-spence-ebaaec/deploys)
# DreamShare landing page Boilerplate using webpack 4

Live DEMO: [https://wizardly-spence-ebaaec.netlify.app/](https://wizardly-spence-ebaaec.netlify.app/)

This is the landing page page for Dreamshare. No Javascript/Jquery used.

NOTE: The `app.bundle.js` is added via webpack during deployment to process the sass file.


This boilerplate can be used as an scaffold to integrate a online template or to start a site from scratch.

## What's included?
- Local Development Server with Hot Reloading
- Template Engine Compilation (using [nunjucks](https://mozilla.github.io/nunjucks/))
- Full support for HTML5, JavaScript (Vanilla and ES6)
- SASS Compilation
- HTML Compression.

## Getting Started

All you need to do is to place static assets under the `/src/assets` folder. All this files will be copied "as is" in the build step and placed in `dist/` folder. Webpack will not compile or transform any these style or script files.

You have a base `_layout.html` file where you can add all the template assets (styles and script). You can have as many layout as you want. Just use in the desired pages.

```html
<!-- all root based path -->
<link rel="stylesheet" href="/assets/css/styles.css" />
<script src="/assets/js/jquery-1.12.4.min.js"></script>
```

Each page should live under `src/templates/pages` (index, about, contact, etc). All pages should "extend" the `_layout.html` in order to share the base structure.

We included a header.html and a footer.html under `src/templates/partials`. Those files could be "included" in order to be used wherever you need them. They could be included in the `_layout.html` file or included in any page file. This should vary according your needs.

Finally any custom styles should be made in `src/scss/main.scss`

Any custom script should be placed inside `src/js/app.js` which will be injected to the html pages.

### Notes

- Each time a new page is added the development server should be restarted.
- Assets path to images should be modified to be `<img src="/assets/..."` in order to find the right path.
- `main.scss` and `app.js` are meant to override any style/scripts from assets folder.

If no template is going to be integrated:

- all your styles should live in the `src/sass/main.scss` file.
- images and fonts should live under `src/assets` folder.
- Custom js should live or imported in `src/js/app.js` file.

### Dependencies (dev)

- You’ll need to have Node >= 14 on your local development machine**. You can use [nvm](http://nvm.sh/)  (macOS/Linux) to easily switch Node versions between different projects.

Beside node, you will need the below dependencies for this project:

- Webpack = 4
- Nunjucks
- PostCss
- Autoprefixer
- NodeSass

### Installing

- Clone this repository.

- Install the dev dependencies using `npm ci` command.

### Executing program

- Start the development server

```bash
npm run start:dev
```

Runs the app in development mode. Please open http://localhost:9000 to view it in the browser. The page will automatically reload if you make changes to the code.

- For static assets production release, run the below command:

```bash
npm run build
```

### Pushing to hosting service / production environment.

If the site is going to be pushed live as single static files. Simple run `npm run build` and upload the content generated in the `dist` folder.

If using a PaaS like Netlify/Vercel/Heroku. Set build command to run `npm run build` and serve the `dist` folder.

### Nunjucks template engine

We use  [nunjucks](https://mozilla.github.io/nunjucks/) as a template engine based on jinja2. A simple yet powerful template engine that allow us to:

- Build partial components to be used in our templates.
- Extend base html structures.
- Basic programming layer to conditionally render block, classes.

More info about template directives [here](https://mozilla.github.io/nunjucks/templating.html).


### Browser Support

- Chrome (latest 2)
- Edge (latest 2)
- Firefox (latest 2)
- Internet Explorer 9+
- Opera (latest 2)
- Safari (latest 2)

### Boilerplate by @elotgamu


